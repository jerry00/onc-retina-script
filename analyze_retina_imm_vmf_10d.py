import pandas as pd
import numpy as np
from scphere.util.util import read_mtx
from scphere.util.trainer import Trainer
from scphere.util.plot import plot_trace
from scphere.model.vae import SCPHERE
# import matplotlib.pyplot as plt
from datetime import datetime

H_DIM = 128
Z_DIM = 10

x = read_mtx('/Users/jding/work/onc-retina/data/retina_imm_v1.mtx')
x = x.transpose().todense()

model = SCPHERE(n_gene=x.shape[1], n_batch=[7, 2],
                z_dim=Z_DIM, latent_dist='vmf',
                observation_dist='nb', seed=0)

batch_t = pd.read_csv('/Users/jding/work/onc-retina/data/batch_time.tsv', header=None)
batch_s = pd.read_csv('/Users/jding/work/onc-retina/data/batch_strain.tsv', header=None)

batch = pd.concat([batch_t, batch_s], axis=1)

trainer = Trainer(model=model, x=x, batch_id=batch.values, max_epoch=500,
                  mb_size=128, learning_rate=0.001)

start = datetime.now()
trainer.train()
print('Time used for training: {}\n'.format(datetime.now() - start))


df = pd.DataFrame(list(zip(trainer.status['log_likelihood'],
                           trainer.status['kl_divergence'],
                           trainer.status['elbo']
                           )),
                  columns=['log_likelihood', 'kl_divergence', 'elbo'])

save_path = '/Users/jding/work/onc-retina/output/'

df.to_csv(save_path + 'trace_vmf_10d_retina_imm_v1.tsv',
          sep=' ', index=False)

z_mean = model.encode(x, batch.values)
np.savetxt(save_path +
           'latent_vmf_10d_retina_imm_v1.tsv',
           z_mean)

ll = model.get_log_likelihood(x, batch.values)
np.savetxt(save_path +
           'll_vmf_10d_retina_imm_v1.tsv',
           ll)

model_name = save_path + 'model_vmf_10d_retina_imm_v1'
model.save_sess(model_name)

